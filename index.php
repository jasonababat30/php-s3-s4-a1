<?php require './code.php' ?>
<!DOCTYPE html>
<html>
<head>
	<title>PHP OOP | Activity</title>
</head>
<body>
	<h1>PHP OOP | Activity</h1>

	<ul>
		<li>
			Print Product here:

			<pre>
				<?php print_r($product); ?>
			</pre>

			<p>
				<?php echo $product->printProductDetails(); ?>
			</p>

			<?php $product->setStockNo(3); ?>

			<p>
				<?php echo "New product stock number:"." ".$product->getStockNo(); ?>
			</p>
		</li>

		<li>
			Print a sub product here:

			<pre>
				<?php print_r($mobile); ?>
			</pre>

			<p>
				<?php echo $mobile->printMobileDetails(); ?>
			</p>

			<?php $mobile->setStockNo(5); ?>

			<p>
				<?php echo "New mobile stock number:"." ".$mobile->getStockNo(); ?>
			</p>
		</li>

		<li>
			Print another sub product here:

			<pre>
				<?php print_r($computer); ?>
			</pre>

			<p>
				<?php echo $computer->printComputerDetails(); ?>
			</p>

			<?php $computer->setCategory("Laptops, Computers, and Electronics"); ?>

			<p>
				<?php echo "New computer category:"." ".$computer->getCategory(); ?>
			</p>
		</li>

	</ul>
</body>
</html>