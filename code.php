<?php 
	
	/*
		Activity:

		1. (Base Class) Create a Product class with five properties: name, price, short description, category, stock numbers.

		2. Create two child classes for Product class: Mobile and Computer.

		3. Create a method called printDetails in each of the classes and must have the following input:

			Product: "The product has a name of <productName> and its price is <productPrice>, and the stock number is <productStock>."

			Mobile: "Our latest mobile is <mobileName> with a cheapest price of <mobilePrice>".

			Computer: "Our newest computer is <computerName> and its base price is <computerPrice>".

		4. On the Product class, create the following getters and setters:

			price
			stock no.
			category

		5. Kindly create an instance of the following:

			Product:
				name - Xiaomi Mi Monitor
				price - 22,000.00
				short description - Good for gaming and for coding.
				stock no - 5
				category - computer-peripherals

			Mobile:
				name - Xiaomi Redmi Note 10 pro
				price - 13590.00
				short description - Latest Xiaomi Phone ever made
				stock no - 10
				category - mobiles and electronics

			Computer:
				name - HP Business Laptop
				price - 29000.00
				short description - HP Laptop only made for business
				stock no - 10
				category- Laptops and Computers

		6. Display the details of an instance of a class of the ff:

			Product - display it on the first list item of the ul tag in the index.php
			Mobile - display it on the second list item of the ul tag in the index.php
			Computer - display it on the last list item of the ul tag in the index.php

		7. Kindly update the following details of the objects.

			Product object - update its stock no from 5 to 3
			Mobile object - update the stock no. from 10 to 5
			Computer Object - update the category from 'laptops and computers' to 'laptops, computers, and electronics.'
	*/

	// No. 1
	class Product{

		public $name;
		public $price;
		public $shortDescription;
		public $category;
		public $stockNo;

		public function __construct($nameValue, $priceValue, $shortDescriptionValue, $categoryValue, $stockNoValue){

			$this->name = $nameValue;
			$this->price = $priceValue;
			$this->shortDescription = $shortDescriptionValue;
			$this->category = $categoryValue;
			$this->stockNo = $stockNoValue;

		}

		// No. 3
		public function printProductDetails(){

			return "The product has a name of $this->name and its price is $this->price, and the stock number is $this->stockNo.";

		}

		// No. 4

		// Getters
		public function getPrice(){
			return $this->price;
		}

		public function getStockNo(){
			return $this->stockNo;
		}

		public function getCategory(){
			return $this->category;
		}

		// Setters
		public function setPrice($newPriceValue){
			$this->price = $newPriceValue;
		}

		public function setStockNo($newStockNoValue){
			$this->stockNo = $newStockNoValue;
		}

		public function setCategory($newCategoryValue){
			$this->category = $newCategoryValue;
		}
	}

	// No. 5
	$product = new Product("Xiaomi Mi Monitor", 22000.00, "Good for gaming and for coding.","Computer-peripherals", 5);

	// No. 2

	class Mobile extends Product{

		// No. 3
		public function printMobileDetails(){

			return "Our latest mobile is $this->name with a cheapest price of $this->price.";

		}

	}

	// No. 5
	$mobile = new Mobile("Xiaomi Redmi Note 10 Pro", 13590.00, "Latest Xiaomi Phone ever made!", "Mobiles and Electronices", 10);
	
	class Computer extends Product{

		// No. 3
		public function printComputerDetails(){

			return "Our newest computer is $this->name and its base price is $this->price.";

		}

	}

	// No. 5
	$computer = new Computer("HP Business Laptop", 29000.00, "HP Laptop only made for business.", "Laptops and Computers", 10);
	

 ?>